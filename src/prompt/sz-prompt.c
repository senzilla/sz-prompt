/* SPDX-License-Identifier: ISC */

#include <skalibs/sig.h>
#include <sz-prompt/prompt.h>

int
main(int argc, char *argv[])
{
	sig_altignore(SIGTSTP) ;
	sig_altignore(SIGQUIT) ;
	sig_altignore(SIGINT) ;
	sz_prompt *p ;
	p = sz_execline_prompt() ;
	return sz_prompt_init(argv, p) ;
}
