/* SPDX-License-Identifier: ISC */

#include <sz-prompt/prompt.h>

int
sz_prompt_init(char *argv[], sz_prompt *p)
{
	EditLine *el ;
	Tokenizer *tok ;
	History *h ;
	HistEvent he ;
	h = history_init() ;
	history(h, &he, H_SETSIZE, 100) ;
	tok = tok_init(NULL) ;
	el = el_init(*argv, stdin, stdout, stderr) ;
	el_set(el, EL_PROMPT, p->ps) ;
	el_set(el, EL_HIST, history, h) ;
	el_set(el, EL_SIGNAL, 0) ;
	el_source(el, NULL) ;
	return sz_prompt_run(el, tok, h, he, p) ;
}
