/* SPDX-License-Identifier: ISC */

#include <sz-prompt/prompt.h>

int
sz_prompt_run(EditLine *el, Tokenizer *tok, History *h, HistEvent he, sz_prompt *p)
{
	int i ;
	char const *buf ;
	while ((buf = el_gets(el, &i)) != NULL && i != 0)
	{
		if (!*p->cont && i == 1) continue; /* XXX: Line feed */
		int argc = 0, cc = 0, co = 0 ;
		const char **argv ;
		const LineInfo *li ;
		li = el_line(el) ;
		*p->cont = tok_line(tok, li, &argc, &argv, &cc, &co) ;
		history(h, &he, *p->cont ? H_APPEND : H_ENTER, buf) ;
		if (*p->cont) continue; /* XXX: Open quotes */
		if (el_parse(el, argc, argv) == -1) p->run(argc, argv) ;
		tok_reset(tok) ;
	}
	el_end(el) ;
	tok_end(tok) ;
	history_end(h) ;
	sz_prompt_end(p) ;
	return 0 ;
}
