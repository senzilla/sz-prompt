/* SPDX-License-Identifier: ISC */

#include <skalibs/alloc.h>
#include <sz-prompt/prompt.h>

static int continuation ;

static char *
sz_execline_ps(EditLine *el)
{
	static char ps1[] = "execline> " ;
	static char ps2[] = "> " ;
	return (continuation ? ps2 : ps1) ;
}

sz_prompt *
sz_execline_prompt()
{
	sz_prompt *p = (sz_prompt *) alloc(sizeof(*p)) ;
	p->cont = &continuation ;
	p->ps = sz_execline_ps ;
	p->run = sz_execline_spawn ;
	return p ;
}
