/* SPDX-License-Identifier: ISC */

#include <skalibs/stralloc.h>

void
sz_argv2sa(int argc, const char **argv, stralloc *sa)
{
	for (int i = 0; i < argc; i++)
	{
		stralloc_catb(sa, argv[i], strlen(argv[i])) ;
		if (i != (argc-1)) stralloc_catb(sa, " ", 1) ;
	}
}
