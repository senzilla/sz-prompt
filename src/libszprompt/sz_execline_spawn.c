/* SPDX-License-Identifier: ISC */

#include <skalibs/djbunix.h>
#include <skalibs/strerr2.h>
#include <sz-prompt/exec.h>

void
sz_execline_spawn(int argc, const char *argv[])
{
	stralloc cmd = STRALLOC_ZERO ;
	int wstat ;
	sz_argv2sa(argc, argv, &cmd) ;
	char const *newargv[] = {"execlineb", "-Pc", cmd.s, 0} ;
	pid_t pid = child_spawn0(newargv[0], newargv, (char const **)environ) ;
	stralloc_free(&cmd) ;
	if (!pid) strerr_warnwu2sys("spawn ", newargv[0]) ;
	if (wait_pid(pid, &wstat) < 0) strerr_warnwu1sys("wait_pid") ;
}
