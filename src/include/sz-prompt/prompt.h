/* SPDX-License-Identifier: ISC */

#include <skalibs/alloc.h>
#include <skalibs/djbunix.h>
#include <skalibs/strerr.h>
#include <histedit.h>

/* Interface */

typedef struct sz_prompt sz_prompt ;

struct sz_prompt
{
	int *cont ;
	char *(*ps)(EditLine *) ;
	void (*run)(int, const char *[]) ;
} ;

int sz_prompt_init(char *[], sz_prompt *) ;

int sz_prompt_run(EditLine *, Tokenizer *, History *, HistEvent, sz_prompt *) ;

#define sz_prompt_end(a) alloc_free(a)

/* Implementation */

sz_prompt * sz_execline_prompt() ;

void sz_execline_spawn(int, const char *[]) ;
