sz-prompt - execline prompt
===========================

Small and simple command line prompt for the execline script language.
It's meant to be used on systems where no interactive shells are available.
